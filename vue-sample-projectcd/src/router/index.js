import { createRouter, createWebHistory } from 'vue-router'
import TopView from '../views/TopView.vue'
import AllblogView from '../views/AllblogView.vue'
import MyblogView from '../views/MyblogView.vue'
import FavoliteView from '../views/FavoliteView.vue'
import CreateView from '../views/CreateView.vue'
import HomeView from '../views/HomeView.vue'
import SigninView from '../views/SigninView.vue'
import SignupView from '../views/SignupView.vue'

const routes = [
  {
    path: '/',
    name: 'top',
    component: TopView
  },
  {
    path: '/allblog',
    name: 'allblog',
    component: AllblogView
  },
  {
    path: '/myblog',
    name: 'myblog',
    component: MyblogView
  },
  {
    path: '/favolite',
    name: 'favolite',
    component: FavoliteView
  },
  {
    path: '/create',
    name: 'create',
    component: CreateView
  },
  {
    path: '/home',
    name: 'home',
    component: HomeView
  },
  {
    path: '/signin',
    name: 'signin',
    component: SigninView
  },
  {
    path: '/signup',
    name: 'signup',
    component: SignupView
  },
  {
    path: '/:matchAll(.*)',
    name: 'notFound',
    component: TopView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
