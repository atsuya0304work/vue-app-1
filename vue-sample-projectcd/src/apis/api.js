import axios from 'axios'

export default {
    loginUser(obj) {
      console.log(process.env.VUE_APP_API_URL_BASE + '/loginUser')
      return axios.post(process.env.VUE_APP_API_URL_BASE + '/loginUser',
        { 
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: {
            userId: obj.userId,
            password: obj.password
          }, 
        }
      )    
    },
    createUser(obj) {
      return axios.post(process.env.VUE_APP_API_URL_BASE + '/createUser',
        { 
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: obj 
        }
      ) 
    },
    getBlogs() {
      return axios.get(process.env.VUE_APP_API_URL_BASE + '/blogs', { 
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
    },
    createBlog(obj) {
      return axios.post(process.env.VUE_APP_API_URL_BASE + '/createBlog',
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: obj
        }
      )
    },
    updateBlog(updateBlog) {
      return axios.post(process.env.VUE_APP_API_URL_BASE + '/updateBlog',
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          }, 
          body: {
            id: updateBlog.id,
            body: updateBlog.body,
            open: updateBlog.open,
            title: updateBlog.title
          }
        })
    },
    deleteBlog(id) {
      return axios.post(process.env.VUE_APP_API_URL_BASE + '/deleteBlog', { 
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: {
          "id": id
        }, 
      })
    },
    updateFavorite(obj) {
      return axios.post(process.env.VUE_APP_API_URL_BASE + '/updateFavorite', { 
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: {
          "blogId": obj.blogId,
          "userId": obj.userId,
          "isLiked": obj.isLiked
        }, 
      })
    }
}
