import MUTATION_TYPES from './mutation-types'

export const mutations = {
    [MUTATION_TYPES.LOGIN_USER_REQUEST] (state) {
        state.isFetching = true
        state.error = null
    },
    [MUTATION_TYPES.LOGIN_USER_SUCCESS] (state, body) {
        state.isFetching = false
        state.data = {
            userId: body.userId,
            username: body.username,
            email: body.email,
            favorite: body.favorite
        }
        state.resultMessage = "ログインに成功しました。"
    },
    [MUTATION_TYPES.LOGIN_USER_FAILURE] (state, e) {
        state.isFetching = false
        state.data = null
        state.error = e.name
    },
    [MUTATION_TYPES.CREATE_USER_REQUEST] (state) {
        state.isFetching = true
        state.error = null
    },
    [MUTATION_TYPES.CREATE_USER_SUCCESS] (state, obj) {
        state.isFetching = false
        state.data = {
            userId: obj.userId,
            username: obj.username,
            email: obj.email,
            favorite: []
        }
        state.resultMessage = "ユーザー登録に成功しました。"
    },
    [MUTATION_TYPES.CREATE_USER_FAILURE] (state, e) {
        state.isFetching = false
        state.data = null
        state.error = e.name
    },
    [MUTATION_TYPES.USER_LOGOUT] (state) {
        state.data = null
        state.resultMessage = "ログアウトしました。"
    },
    [MUTATION_TYPES.UPDATE_FAVORITE_REQUEST] (state) {
        state.isFetching = true
        state.error = null
    },
    [MUTATION_TYPES.UPDATE_FAVORITE_SUCCESS] (state, obj) {
        state.isFetching = false
        state.resultMessage = obj.isLiked ? "お気に入りを解除しました。"
                                          : "お気に入りを追加しました。"
        if (obj.isLiked) {
            state.data.favorite = state.data.favorite.filter(function(id) {
                return id !== obj.blogId
            })
        } else {
            state.data.favorite.push(obj.blogId)
        }
    },
    [MUTATION_TYPES.UPDATE_FAVORITE_FAILURE] (state, e) {
        state.isFetching = false
        state.data = null
        state.error = e.name
    },
}