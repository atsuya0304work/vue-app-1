import actions from './action'
import { mutations } from './mutation'

export default {
    state: {
        isFetching: false,
        data: null,
        error: null,
        resultMessage: ""
    },
    mutations,
    actions,
}