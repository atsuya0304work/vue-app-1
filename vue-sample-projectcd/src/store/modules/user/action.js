import Api from '../../../apis/api'
import MUTATION_TYPES from './mutation-types'

export default {
    createUser({ commit }, obj) {
        commit(MUTATION_TYPES.CREATE_USER_REQUEST)
        Api.createUser(obj)
        .then(function(res) {
            const data = res.data
            const statusCode = data.statusCode
            const body = JSON.parse(data.body)
            // console.log(res)
            // console.log(statusCode)
            // console.log(body)
            if (statusCode !== 200) {
               throw new ServerError(body)
            }
            commit(MUTATION_TYPES.CREATE_USER_SUCCESS, obj)
        })
        .catch(function(e) {
            commit(MUTATION_TYPES.CREATE_USER_FAILURE, e)
        })
    },
    loginUser({ commit }, obj) {
        commit(MUTATION_TYPES.LOGIN_USER_REQUEST)
        Api.loginUser(obj)
        .then(function(res) {
            console.log(res)
            const data = res.data
            const statusCode = data.statusCode
            const body = JSON.parse(data.body)
            console.log(statusCode)
            console.log(body)
            if (statusCode !== 200) {
               throw new ServerError(body)
            }
            commit(MUTATION_TYPES.LOGIN_USER_SUCCESS, body)
        })
        .catch(function(e) {
            console.log(e)
            commit(MUTATION_TYPES.LOGIN_USER_FAILURE, e)
        })
    },
    logoutUser({ commit }) {
        commit(MUTATION_TYPES.USER_LOGOUT)
    },
    updateFavorite({ commit }, obj) {
        commit(MUTATION_TYPES.UPDATE_FAVORITE_REQUEST)
        console.log(obj)
        Api.updateFavorite(obj)
        .then(function(res) {
            const data = res.data
            const statusCode = data.statusCode
            const body = JSON.parse(data.body)
            // console.log(res)
            // console.log(statusCode)
            // console.log(body)
            if (statusCode !== 200) {
               throw new ServerError(body)
            }
            console.log(obj.isLiked)
            commit(MUTATION_TYPES.UPDATE_FAVORITE_SUCCESS, obj)
        })
        .catch(function(e) {
            // console.log(e)
            commit(MUTATION_TYPES.UPDATE_FAVORITE_FAILURE, e)
        })
    }
}

class ServerError extends Error {
    constructor(obj) {
      super(obj.errCode);
      this.name = obj.errMessage;
    }
}
