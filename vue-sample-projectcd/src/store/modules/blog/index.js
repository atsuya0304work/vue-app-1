import actions from './action'
import { mutations } from './mutation'

export default {
    state: {
        isFetching: false,
        allblogs: [],
        error: null,
        resultMessage: ""
    },
    mutations,
    actions
}
