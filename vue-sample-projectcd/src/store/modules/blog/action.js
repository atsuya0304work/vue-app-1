import Api from '../../../apis/api'
import MUTATION_TYPES from './mutation-types'


const getBlogs = function({commit}, isCall) {
    if (!isCall) {
        return
    }
    commit(MUTATION_TYPES.GET_BLOGS_REQUEST)
    Api.getBlogs()
    .then(function(res) {
        const data = res.data
        const statusCode = data.statusCode
        const body = JSON.parse(data.body)
        console.log(res)
        console.log(statusCode)
        console.log(body)
        if (statusCode !== 200) {
           throw new ServerError(body)
        }
        commit(MUTATION_TYPES.GET_BLOGS_SUCCESS, body)
    })
    .catch(function(e) {
        commit(MUTATION_TYPES.GET_BLOGS_FAILURE, e)
    })
}

export default {
    getBlogs({commit}, isCall = false) {
        getBlogs({commit}, isCall)
    },
    createBlog({commit}, obj) {
        commit(MUTATION_TYPES.CREATE_BLOG_REQUEST)
        console.log(obj)
        Api.createBlog(obj)
        .then(function(res) {
            const data = res.data
            const statusCode = data.statusCode
            const body = JSON.parse(data.body)
            // console.log(res)
            // console.log(statusCode)
            // console.log(body)
            if (statusCode !== 200) {
               throw new ServerError(body)
            }
            commit(MUTATION_TYPES.CREATE_BLOG_SUCCESS)
            getBlogs({commit}, true)
        })
        .catch(function() {
            commit(MUTATION_TYPES.CREATE_BLOG_FAILURE)
        })
    },
    updateBlog({commit}, updateBlog) {
        commit(MUTATION_TYPES.UPDATE_BLOG_REQUEST)
        Api.updateBlog(updateBlog)
        .then(function(res) {
            const data = res.data
            const statusCode = data.statusCode
            const body = JSON.parse(data.body)
            console.log(res)
            console.log(statusCode)
            console.log(body)
            if (statusCode !== 200) {
               throw new ServerError(body)
            }
            commit(MUTATION_TYPES.UPDATE_BLOG_SUCCESS)
            getBlogs({commit}, true)
        })
        .catch(function(e) {
            commit(MUTATION_TYPES.UPDATE_BLOG_FAILURE, e)
        })
    },
    deleteBlog({commit}, id) {
        commit(MUTATION_TYPES.DELETE_BLOG_REQUEST)
        Api.deleteBlog(id)
        .then(function(res) {
            const data = res.data
            const statusCode = data.statusCode
            const body = JSON.parse(data.body)
            console.log(res)
            console.log(statusCode)
            console.log(body)
            if (statusCode !== 200) {
               throw new ServerError(body)
            }
            commit(MUTATION_TYPES.DELETE_BLOG_SUCCESS)
            getBlogs({commit}, true)
        })
        .catch(function(e) {
            commit(MUTATION_TYPES.DELETE_BLOG_FAILURE, e)
        })
    }
}

class ServerError extends Error {
    constructor(obj) {
      super(obj.errCode);
      this.name = obj.errMessage;
    }
}
