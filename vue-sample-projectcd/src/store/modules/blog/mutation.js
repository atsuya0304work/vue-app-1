import MUTATION_TYPES from './mutation-types'

export const mutations = {
    [MUTATION_TYPES.GET_BLOGS_REQUEST] (state) {
        state.isFetching = true
        state.error = null
    },
    [MUTATION_TYPES.GET_BLOGS_SUCCESS] (state, data) {
        state.isFetching = false
        state.allblogs = data
        state.error = null
    },
    [MUTATION_TYPES.GET_BLOGS_FAILURE] (state, e) {
        state.isFetching = false
        state.error = e
    },
    [MUTATION_TYPES.CREATE_BLOG_REQUEST] (state) {
        state.isFetching = true
        state.resultMessage = ""
        state.error = null
    },
    [MUTATION_TYPES.CREATE_BLOG_SUCCESS] (state) {
        state.isFetching = false
        state.resultMessage = "投稿に成功しました。"
    },
    [MUTATION_TYPES.CREATE_BLOG_FAILURE] (state, e) {
        state.isFetching = false
        state.error = e
    },
    [MUTATION_TYPES.UPDATE_BLOG_REQUEST] (state) {
        state.isFetching = true
        state.resultMessage = ""
        state.error = null
    },
    [MUTATION_TYPES.UPDATE_BLOG_SUCCESS] (state) {
        state.isFetching = false
        state.resultMessage = "更新に成功しました。"
    },
    [MUTATION_TYPES.UPDATE_BLOG_FAILURE] (state, e) {
        state.isFetching = false
        state.error = e
    },
    [MUTATION_TYPES.DELETE_BLOG_REQUEST] (state) {
        console.log('request')
        state.isFetching = true
        state.resultMessage = ""
        state.error = null
    },
    [MUTATION_TYPES.DELETE_BLOG_SUCCESS] (state) {
        console.log('success')
        state.isFetching = false
        state.resultMessage = "削除に成功しました。"
    },
    [MUTATION_TYPES.DELETE_BLOG_FAILURE] (state, e) {
        console.log(e)
        state.isFetching = false
        state.error = e
    },
}