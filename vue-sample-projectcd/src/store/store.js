import { createStore } from 'vuex'
import blog from './modules/blog'
import user from './modules/user'
import createPersistedState from 'vuex-persistedstate'

export default createStore({
    modules: {
        blog: blog,
        user: user
    },
    plugins: [createPersistedState({
        key: 'vue-app-1',
        path: [
            'user.data'    
        ]
    })],
    storage: window.sessionStorage
})